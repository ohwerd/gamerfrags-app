//
//  AppDelegate.h
//  Gamerfrags App
//
//  Created by Edward Crupi on 31/03/14.
//  Copyright (c) 2014 Two Bulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
