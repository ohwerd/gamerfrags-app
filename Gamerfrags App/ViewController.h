//
//  ViewController.h
//  Gamerfrags App
//
//  Created by Edward Crupi on 31/03/14.
//  Copyright (c) 2014 Two Bulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
